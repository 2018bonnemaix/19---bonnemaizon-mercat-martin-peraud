def convertir_donnees_acquisition(masse,position_soleil,position_planete,extremite_vecteur,facteur_multiplicatif_distance=1,facteur_multiplicatif_vitesse=1e-7,facteur_multiplicatif_masse=1):
    """
    :param masse: de la planete
    :param position_soleil: origine de l'univers
    :param position_planete: tuple de coordonnées
    :param extremite_vecteur:
    :param facteur_multiplicatif_distance:
    :param facteur_multiplicatif_vitesse:
    :param facteur_multiplicatif_masse:
    :return: Convertit les données d'acquisition avec des facteurs multiplicatifs pour la partie jeu à n bodies
    """
    nouvelle_pos_planete=((position_planete[0]-position_soleil[0])*facteur_multiplicatif_distance,(position_planete[1]-position_soleil[1])*facteur_multiplicatif_distance)
    vecteur_vitesse=((extremite_vecteur[0]-position_planete[0])*facteur_multiplicatif_vitesse,(extremite_vecteur[1]-position_planete[1])*facteur_multiplicatif_vitesse)
    return masse*facteur_multiplicatif_masse,nouvelle_pos_planete, vecteur_vitesse

