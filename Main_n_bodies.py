from game_of_planets.for_n_bodies import *
from game_of_planets.Interface_plusieurs_planetes import *
from game_of_planets.animation_n_bodies import *

def lancement_n_bodies():
    """
    :return: lance la simulation en mode jeu en utilisant toutes les fonctions (interface utilisateur,schéma numérique et animation)
    """
    univers=acquisition()
    animation_n_bodies(univers,20,20,True)
    return True

if __name__=='__main__':
    lancement_n_bodies()
