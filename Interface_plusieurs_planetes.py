import random as rd
import pygame
from game_of_planets.convertir_donnees_acquisition import *
import numpy as np


# Lancer la simulation.
# Rentrer la masse de la planete (rien ne s'affiche) Appuyer sur e sur valider
# Placer la planete, appuyer sur p pour valider sa position, faire de meme avec le vecteur vitesse
#pour ajouter une planete, appuyer sur a et recommencer
#fermer la fenetre avec la touche f

#Retourne l'univers

def rayon(Masse):
    return int((Masse/100)**(1/3)*18+2)

def acquerir_la_masse(ecran,largeur):
    """
    Demande a l'utilisateur de rentrer la masse de la planete, la renvoie
    :param ecran:
    :param largeur:
    :return:
    """
    myfont = pygame.font.SysFont('Arial', 15)
    Masse=''
    masse=False
    continuer=True
    textsurface = myfont.render('Masse: ', False, (255,255,255))
    textconsigne = myfont.render('Appuyer sur e pour valider',False,(255,255,255))
    ecran.blit(textsurface,(0,0))
    ecran.blit(textconsigne,(largeur//2,0))
    pygame.display.flip()
    while continuer and not masse:
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                carac= event.dict['unicode']
                if carac in ['0','1','2','3','4','5','6','7','8','9','.']:
                    Masse+=carac
                    textsurface = myfont.render('Masse: '+Masse, False, (255,255,255))
                    ecran.blit(textsurface,(0,0))
                    pygame.display.flip()
                elif event.key == pygame.K_f:
                    continuer=False
                elif event.key == pygame.K_e:
                    Masse=float(Masse)
                    masse=True
                    pygame.draw.rect(ecran, (0, 0, 0),pygame.Rect(0,0,largeur,20),0)
                    pygame.display.flip()
    if masse:
        return Masse
    pygame.quit()

def dynamique_planete(ecran, pos_soleil,couleur,Masse,largeur,univers):
    myfont = pygame.font.SysFont('Arial', 15)
    Rayon=rayon(Masse)
    textconsigne = myfont.render('Appuyer sur p pour valider la position',False,(255,255,255))
    ecran.blit(textconsigne,(0,0))
    pos_planete=(pos_soleil[0]+100,pos_soleil[1])
    pos_planete_prec=pos_planete
    extremite_vecteur=(0,0)
    extremite_vecteur_prec=(0,0)
    continuer=True
    planete_placee=False
    vecteur_vitesse=False
    while continuer and not planete_placee:
        for event in pygame.event.get():
            pygame.draw.circle(ecran,couleur,pos_planete,Rayon)
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_f:# on ferme la fenetre
                    continuer = False
                if event.key == pygame.K_p: #la position de la planete est validee
                    planete_placee=True
                    extremite_vecteur=pos_planete
            if event.type == pygame.MOUSEMOTION: #la planete suit la position de la souris
                pos_planete_prec = pos_planete#on conserve l'ancienne position pour l'effacer
                pos_planete = event.pos
            pygame.draw.circle(ecran,(0,0,0),pos_planete_prec,Rayon)
            pygame.draw.circle(ecran,couleur,pos_planete,Rayon)
            pygame.display.flip()
    while continuer and planete_placee and not vecteur_vitesse:
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_f:#fermer la fenetre
                    continuer = False
                if event.key ==pygame.K_p:#valider la position du vecteur vitesse
                    vecteur_vitesse=True
            if event.type == pygame.MOUSEMOTION: ##l'extremite du vecteur vitesse suit la souris
                extremite_vecteur_prec=extremite_vecteur
                extremite_vecteur=event.pos
            pygame.draw.line(ecran,(0,0,0),pos_planete,extremite_vecteur_prec,2)    ##on efface le dessin precedent en le recouvrant de noir
            pygame.draw.circle(ecran,couleur,pos_planete,Rayon)
            pygame.draw.line(ecran,(200,0,0),pos_planete,extremite_vecteur,2)
            pygame.display.flip()
    pygame.draw.rect(ecran, (0, 0, 0),pygame.Rect(0,0,largeur,20),0)
    textconsigne = myfont.render('Appuyer sur a pour ajouter une planete, f pour fermer',False,(255,255,255))
    ecran.blit(textconsigne,(0,0))
    pygame.display.flip()
    return convertir_donnees_acquisition(Masse, pos_soleil, pos_planete, extremite_vecteur)

def ajouter_planete(ecran, pos_soleil,univers,couleur,largeur):
    Masse=acquerir_la_masse(ecran,largeur)
    Masse, pos_planete, vecteur_vitesse=dynamique_planete(ecran,pos_soleil,couleur,Masse,largeur,univers)
    univers.append([Masse,np.array(pos_planete,dtype=float),np.array(vecteur_vitesse,dtype=float),couleur])
    return univers

def acquisition(taille_ecran=(600,400)):
    pygame.init()
    pygame.font.init() # you have to call this at the start,
                   # if you want to use this module.
    largeur=taille_ecran[0]
    ecran = pygame.display.set_mode((600,400))
    pos_soleil=(taille_ecran[0]//2,taille_ecran[1]//2)
    univers=[]
    couleur=(rd.randint(20,250),rd.randint(20,250),rd.randint(20,250))
    univers=ajouter_planete(ecran,pos_soleil,univers,couleur,largeur)
    continuer=True
    while continuer:
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_f:
                    continuer=False
                if event.key == pygame.K_q:
                    pygame.draw.rect(ecran, (0, 0, 0),pygame.Rect(0,0,largeur,20),0)
                    pygame.display.flip()
                    couleur=(rd.randint(20,250),rd.randint(20,250),rd.randint(20,250))
                    univers=ajouter_planete(ecran,pos_soleil,univers,couleur,largeur)
    return(univers)

