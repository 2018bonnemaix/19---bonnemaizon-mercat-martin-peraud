import numpy as np
G=6.67408*(10**-11)
z=np.array([0,0])
Terre=[6e24,np.array([-150000000000,0],dtype=float),np.array([0,-29820],dtype=float),(17,108,240)]
Soleil=[1.98e30,z,z,(250,250,0)]
Mercure=[0.055*6e24,np.array([-58000000000,0],dtype=float),np.array([0,-47900],dtype=float),(117,122,129)]
Venus=[0.815*6e24,np.array([-108200000000,0],dtype=float),np.array([0,-35900],dtype=float),(242,74,153)]
Mars=[0.107e24,np.array([-229000000000,0],dtype=float),np.array([0,-24100],dtype=float),(250,0,0)]
Lune=[0.0123*6e24, np.array([-150000000000 + 384400000,0],dtype = float),np.array([0,-29820*0.97],dtype=float),(250,250,250)]
Jupiter = [317.9*6e24, np.array([-779.4e9,0],dtype=float), np.array([0,-13100],dtype=float),(0,250,0)]
Saturne = [95.2*6e24, np.array([-1433.9e9,0],dtype=float),np.array([0,-9600],dtype=float),(243,131,49)]
Uranus = [14.6*6e24, np.array([-2877.2e9,0],dtype=float), np.array([0,-6800],dtype=float),(49,243,210)]
Neptune = [17.2*6e24, np.array([-4504.4e9,0],dtype=float),np.array([0,-5400],dtype=float),(193,49,243)]
liste_astres_attracteurs=[Soleil, Mercure, Venus, Terre, Mars, Jupiter, Saturne, Uranus, Neptune, Lune]

def distance(position1, position2):
    """
    Cette fonction prend la position des centres de deux corps et renvoie la distance qui les sépare
    :param position1: Un array de taille (2,) représentant la position du corps 1
    :param position2: Un array de taille (2,) représentant la position du corps 2
    :return: la distance entre ces deux corps (float)
    """
    Xdiff = float(position1[0]-position2[0])
    Ydiff = float(position1[1]-position2[1])
    return np.sqrt(Xdiff**2 + Ydiff**2)


def trouve_theta(vecteur):
    """
    trouve l'angle theta des coordonnés cylindriques pour un point
    :param vecteur:
    :return: un float en radian
    """
    if vecteur[0]>0 :
        theta = np.arctan(vecteur[1]/vecteur[0])
    if vecteur[0]<0:
        theta = np.arctan(vecteur[1]/vecteur[0])+np.pi
    if vecteur[0] == 0:
        theta = 0.5*np.sign(vecteur[1])*np.pi
    return theta


def cylindrique_to_cartesien(vecteur):
    """
    Coord Cylindrique(r, theta) ---->  Cartésien (x, y)
    :param vecteur:
    :return: vecteur
    """
    NewX = float(vecteur[0]*np.cos(vecteur[1]))
    NewY = float(vecteur[0]*np.sin(vecteur[1]))
    return np.array([NewX, NewY])

def cartesien_to_cylindrique(vecteur):
    """
    Coord cartésiennes(x, y) ----> Cylindriques (r, theta)
    :param vecteur:
    :return: vecteur
    """
    NewR = np.sqrt(float(vecteur[0]**2) + float(vecteur[1]**2))
    if vecteur[0]>0:
        Newtheta = np.arctan(float(vecteur[1]/vecteur[0]))
    if vecteur[0]<0 :
        Newtheta = np.pi+np.arctan(float(vecteur[1]/vecteur[0]))
    if vecteur[0]==0:
        if vecteur[1]>0:
            Newtheta = 0.5*np.pi
        if vecteur[1]<0:
            Newtheta = -0.5*np.pi
        if vecteur[1] == 0 :
            Newtheta = 0
    return np.array([NewR, Newtheta])

def Force(planete1, astre_attracteur,jeu=False):
    """
    Renvoie la force exercée par l'astre_attracteur 2 sur la planète 1 SOUS FORME DE VECTEUR
    :param planète1: Liste contenant dans lordre : Masse(int), Position(array), Vitesse(Array)
    :param planète2: idem
    :return: vecteur (coordonnés cartésienne)
    """
    vecteur = np.array([astre_attracteur[1][0]-planete1[1][0],astre_attracteur[1][1]-planete1[1][1]])
    force=G*float(planete1[0]*astre_attracteur[0])*vecteur/(distance(planete1[1], astre_attracteur[1])**3)
    if jeu:
        F = (1e-40)*force
        if abs(F)>= 1e8:
            return np.sign(1e8)
        else:
            return F
    return force

