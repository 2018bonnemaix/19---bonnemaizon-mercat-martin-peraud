import numpy as np
from game_of_planets.for_n_bodies import *

def generate_n_bodies(univers,i,interval,jeu=False):
    """
    genere l'etat suivant pour une planete en prenant en compte toutes les autres
    :param univers: list
    :param i: int
    :param interval: int
    :param jeu: bool
    :return: l'etat suivant de la planete d'indice i
    """
    planete2=univers[i]
    for j in range(interval):
        planete2=calcul_etat_suivant_general(univers,i,jeu)
    return planete2
