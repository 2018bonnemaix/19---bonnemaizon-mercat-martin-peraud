from game_of_planets.useful_functions import *
from game_of_planets.for_n_bodies import *
from game_of_planets.animation_n_bodies import *
from matplotlib import pyplot as plt

#Constantes
G=6.67e-11
z=np.array([0,0])
Terre=[6e24,np.array([-150000000000,0],dtype=float),np.array([0,-29820],dtype=float),(17,108,240)]
Soleil=[1.98e30,z,z,(250,250,0)]
Mercure=[0.055*6e24,np.array([-58000000000,0],dtype=float),np.array([0,-47900],dtype=float),(117,122,129)]
Venus=[0.815*6e24,np.array([-108200000000,0],dtype=float),np.array([0,-35900],dtype=float),(242,74,153)]
Mars=[0.107e24,np.array([-229000000000,0],dtype=float),np.array([0,-24100],dtype=float),(250,0,0)]
Lune=[0.0123*6e24, np.array([-150000000000 + 384400000,0],dtype = float),np.array([0,-29820*0.97],dtype=float),(0,0,0)]
Jupiter = [317.9*6e24, np.array([-779.4e9,0],dtype=float), np.array([0,-13100],dtype=float),(0,250,0)]
Saturne = [95.2*6e24, np.array([-1433.9e9,0],dtype=float),np.array([0,-9600],dtype=float),(243,131,49)]
Uranus = [14.6*6e24, np.array([-2877.2e9,0],dtype=float), np.array([0,-6800],dtype=float),(49,243,210)]
Neptune = [17.2*6e24, np.array([-4504.4e9,0],dtype=float),np.array([0,-5400],dtype=float),(193,49,243)]
#Listes contennat les donnés de tout les astres du système solaire
liste_astres_attracteurs=[Soleil, Mercure, Venus, Terre, Mars, Jupiter, Saturne, Uranus, Neptune, Lune]


def fonction_solar_system():
    """
    :return: Test de la focntion calcul_etat_suivant avec matplotlib
    """
    etat_Mercure, etat_Venus, etat_Terre, etat_Mars, etat_Lune, etat_Jupiter, etat_Saturne, etat_Uranus, etat_Neptune = liste_astres_attracteurs[1],liste_astres_attracteurs[2],liste_astres_attracteurs[3],liste_astres_attracteurs[4], liste_astres_attracteurs[9], liste_astres_attracteurs[5], liste_astres_attracteurs[6], liste_astres_attracteurs[7], liste_astres_attracteurs[8]
    compteur=0
    for n in range(5000):
        compteur+=1
        etat_Mercure, etat_Venus, etat_Terre, etat_Mars, etat_Lune, etat_Jupiter, etat_Saturne, etat_Uranus, etat_Neptune = calcul_etat_suivant_general(liste_astres_attracteurs,1),calcul_etat_suivant_general(liste_astres_attracteurs,2),calcul_etat_suivant_general(liste_astres_attracteurs,3),calcul_etat_suivant_general(liste_astres_attracteurs,4), calcul_etat_suivant_general(liste_astres_attracteurs,9), calcul_etat_suivant_general(liste_astres_attracteurs,5), calcul_etat_suivant_general(liste_astres_attracteurs,6), calcul_etat_suivant_general(liste_astres_attracteurs,7), calcul_etat_suivant_general(liste_astres_attracteurs,8)
        x_T=etat_Terre[1][0]
        y_T=etat_Terre[1][1]
        x_L=etat_Lune[1][0]
        y_L=etat_Lune[1][1]

        x_V=etat_Venus[1][0]
        y_V=etat_Venus[1][1]

        x_Me=etat_Mercure[1][0]
        y_Me=etat_Mercure[1][1]

        x_Ma=etat_Mars[1][0]
        y_Ma=etat_Mars[1][1]

        x_Ju=etat_Jupiter[1][0]
        y_Ju=etat_Jupiter[1][1]

        x_Sa=etat_Saturne[1][0]
        y_Sa=etat_Saturne[1][1]

        x_Ur=etat_Uranus[1][0]
        y_Ur=etat_Uranus[1][1]

        x_Ne=etat_Neptune[1][0]
        y_Ne=etat_Neptune[1][1]

        if compteur==100:
            compteur=0
            plt.plot(x_Ma,y_Ma,"o",color = tuple([i/255 for i in Mars[3]]))
            plt.plot(x_Me,y_Me,"o", color = tuple([i/255 for i in Mercure[3]]) )
            plt.plot(x_V,y_V,"o",color = tuple([i/255 for i in Venus[3]]))
            plt.plot(x_T,y_T,"o",color = tuple([i/255 for i in Terre[3]]))
            plt.plot(x_L,y_L,".",color = tuple([i/255 for i in Lune[3]]))
            #plt.plot(x_Ju,y_Ju,"o",color = tuple([i/255 for i in Jupiter[3]]))
            #plt.plot(x_Sa,y_Sa,"o",color = tuple([i/255 for i in Saturne[3]]))
            #plt.plot(x_Ur,y_Ur,"o",color = tuple([i/255 for i in Uranus[3]]))
            #plt.plot(x_Ne,y_Ne,"o",color = tuple([i/255 for i in Neptune[3]]))
            plt.plot(Soleil[1][0],Soleil[1][1],"o",color = tuple([i/255 for i in Soleil[3]]))
    plt.axis("equal")
    plt.grid()
    plt.show()
    return True


#fonction_solar_system()

print(animation_n_bodies(liste_astres_attracteurs[:9],200,200,jeu=False))




