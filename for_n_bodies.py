from game_of_planets.useful_functions import *
import numpy as np

def F_totale(liste_astres, indice_astre,jeu=False):
    """
    #Cette fonction renvoie le vecteur de la force exercée par n corps sur un astre
    :param liste_astres: liste des astres, les astres sont des [masse, vecteur_pos, vecteur_vit]
    :param indice_astre : indice de l'astre sur lequel s'exerce la force
    :return: le vecteur force (un array)
    """
    c=0
    F = np.array([0,0],dtype=float)
    for k in liste_astres:
        #Si on regarde un autre astre, on regarde la force qu'il exerce sur l'astre qui nous intéresse
        if indice_astre != c:
            #...et on l'ajoute
            F+=Force(liste_astres[indice_astre],liste_astres[c],jeu)
        c+=1
    return F


def calcul_etat_suivant_general(liste_astres,indice,jeu=False):
    """
    calcule l'état suivant d'une planete par intégration du PFD
    :param indice : indice de la planete dans la liste
    liste astre : liste de tout les astres
    :return: la nouvelle liste [masse,[new_px,new_py],[new_vx,new_vy]] au temps t+dt
    """
    #pas d'intégration
    Dt=3600 #en secondes
    #Calcul de la force et de l'accélération
    vect_force = F_totale(liste_astres, indice,jeu)
    acc_carte=(1/liste_astres[indice][0])*vect_force
    #Calcul des vitesses et positions (x puis y)
    liste_astres[indice][2][0],liste_astres[indice][1][0]=liste_astres[indice][2][0]+acc_carte[0]*Dt,liste_astres[indice][1][0]+liste_astres[indice][2][0]*Dt+(acc_carte[0]*Dt**2)/2
    liste_astres[indice][2][1],liste_astres[indice][1][1]=liste_astres[indice][2][1]+acc_carte[1]*Dt,liste_astres[indice][1][1]+liste_astres[indice][2][1]*Dt+(acc_carte[1]*Dt**2)/2
    #On retourne la planete[massse,nouvelle_pos,nouvelle_vit]
    return liste_astres[indice]
