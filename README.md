# 19 - Bonnemaizon-Mercat-Martin-Peraud

Projet de la deuxième semaine des coding weeks

L'objectif est de modéliser et représenter l'interaction entre des objets célestes (astres, satellites...)

Découpage en sprints :
1) Code avec un soleil et une planète
2) Code avec un soleil et plusieurs planètes en négligeant l'interaction des planètes entre elles
3) Ajouter des satellites aux planètes
4) Prendre en compte toutes les interactions : on n'est plus obligé d'avoir un soleil lourd et des planètes légères
5) Modèle 3D complet

Fonctionnalités :
0) Fonctionnalités d'usages (outils : conversion systèmes coordonnéess, calcul de distance...)
1) Fonctionnalités d'évolution (cacules des prochains paramètres, calcul des forces...)
2) Fonctionnalités d'intéraction avec l'utilisateur (dans un premier temps : Tkinter, idéalement : une interface graphique sur leqeul l'utilisateur peut
déposer les planètes et dessiner les vecteurs vitessesn initiales)
3) Fonctionnalités d'affichage (dans un premier temps matplotlib, idéalement pygame)

1ère répartition :
1) Intégration des équations différentielles et choix du schéma numérique : Thibaut Martin (+Xavier Bonnemaizon)
2) Fonctionnalités d'intéraction avec l'utilisateur : Flora Mercat
3) Fonctionnalités d'usages et calcul des forces : Xavier Bonnemaizon
4) Fonctionnalités d'affichage : Epi Péraud
