from game_of_planets.generate_n_bodies import *

import pygame as pg
import numpy as np
import time

def animation_n_bodies(univers,speed,iteration,jeu=False):
        """
        affiche le système solaire si jeu=False et le jeu interactif si jeu=True
        :param univers: list
        :param speed: int
        :param iteration: int
        :param jeu: bool
        :return: affiche l'animation choisie
        """
        #fait évoluer l'univers
        def update(univers,iteration,jeu=False):
            univers2=[0]*len(univers)
            for i in range(len(univers)):
                univers2[i]=generate_n_bodies(univers,i,iteration,jeu=False)
            for i in range(len(univers)):
                univers[i][1][0]=univers2[i][1][0]
                univers[i][1][1]=univers2[i][1][1]
                univers[i][2][0]=univers2[i][2][0]
                univers[i][2][1]=univers2[i][2][1]

        #main game function
        def play():
                #initialization
                zoom=100e8
                deplacer=200
                pg.init()
                scrn = pg.display.set_mode((500, 500))
                mainsrf = pg.Surface((500, 500))
                mainsrf.fill((0,0,0))
                pg.display.set_caption('Mouvements des planetes')
                if jeu==False:
                    for i in range(len(univers)):
                        if univers[i][0]==1.98e30:
                            pg.draw.circle(scrn,univers[i][3],(250+deplacer+int(univers[i][1][0]/zoom),250+int(univers[i][1][1]/zoom)),int(((4*univers[i][0]/(3*np.pi))**(1/3)*5)/zoom),0)
                            pg.display.flip()
                        else:
                            pg.draw.circle(scrn,univers[i][3],(250+deplacer+int(univers[i][1][0]/zoom),250+int(univers[i][1][1]/zoom)),int(((4*univers[i][0]/(3*np.pi))**(1/3)*100)/zoom),0)
                            pg.display.flip()
                else:
                    for i in range(len(univers)):
                        radius= int(((univers[i][0])/100)**(1/3)*18+2)
                        pg.draw.circle(scrn,univers[i][3],(250+int(univers[i][1][0]),250+int(univers[i][1][1])), radius,0)
                        pg.display.flip()

                #game cycle
                while 1:
                        #tracking quitting
                        for event in pg.event.get():
                                if event.type == pg.QUIT:
                                        pg.quit()
                                elif event.type==pg.KEYDOWN:
                                    if event.key==pg.K_w and deplacer>15:
                                            zoom=zoom-100e7
                                            deplacer=deplacer-21
                                    elif event.key==pg.K_d and deplacer<200:
                                            zoom=zoom+100e7
                                            deplacer=deplacer+21


                        #drawing
                        if jeu==False:
                            for i in range(len(univers)):
                                if univers[i][0]==1.98e30:
                                    pg.draw.circle(scrn,univers[i][3],(250+deplacer+int(univers[i][1][0]/zoom),250+int(univers[i][1][1]/zoom)),int(((4*univers[i][0]/(3*np.pi))**(1/3)*5)/zoom),0)
                                    pg.display.flip()
                                else:
                                    pg.draw.circle(scrn,univers[i][3],(250+deplacer+int(univers[i][1][0]/zoom),250+int(univers[i][1][1]/zoom)),int(((4*univers[i][0]/(3*np.pi))**(1/3)*100)/zoom),0)
                                    pg.display.flip()
                        else:
                            for i in range(len(univers)):
                                radius= int((univers[i][0]/100)**(1/3)*18+2)
                                pg.draw.circle(scrn, univers[i][3], (int(250+univers[i][1][0]), int(250+univers[i][1][1])), radius, 0)
                                pg.display.flip()
                        pg.time.wait(speed)
                        update(univers,iteration,jeu)
                        scrn.blit(mainsrf,(0,0))
                        pg.display.update()

        play()


