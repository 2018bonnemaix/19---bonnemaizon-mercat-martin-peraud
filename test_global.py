from pytest import *
from game_of_planets.useful_functions import *
from game_of_planets.convertir_donnees_acquisition import *
from game_of_planets.for_n_bodies import *

z=np.array([0,0])
Terre=[6e24,np.array([-150000000000,0],dtype=float),np.array([0,-29820],dtype=float),(17,108,240)]
Soleil=[1.98e30,z,z,(250,250,0)]
Mercure=[0.055*6e24,np.array([-58000000000,0],dtype=float),np.array([0,-47900],dtype=float),(117,122,129)]
Venus=[0.815*6e24,np.array([-108200000000,0],dtype=float),np.array([0,-35900],dtype=float),(242,74,153)]
Mars=[0.107e24,np.array([-229000000000,0],dtype=float),np.array([0,-24100],dtype=float),(250,0,0)]
Lune=[0.0123*6e24, np.array([-150000000000 + 384400000,0],dtype = float),np.array([0,-29820*0.97],dtype=float),(250,250,250)]
Jupiter = [317.9*6e24, np.array([-779.4e9,0],dtype=float), np.array([0,-13100],dtype=float),(0,250,0)]
Saturne = [95.2*6e24, np.array([-1433.9e9,0],dtype=float),np.array([0,-9600],dtype=float),(243,131,49)]
Uranus = [14.6*6e24, np.array([-2877.2e9,0],dtype=float), np.array([0,-6800],dtype=float),(49,243,210)]
Neptune = [17.2*6e24, np.array([-4504.4e9,0],dtype=float),np.array([0,-5400],dtype=float),(193,49,243)]
liste_astres_attracteurs=[Soleil, Mercure, Venus, Terre, Mars, Jupiter, Saturne, Uranus, Neptune, Lune]

def test_convertir_acquisition():
    assert convertir_donnees_acquisition(32,(100,200),(300,450),(500,600))==(1.92e+24, (300000000000.0, 375000000000.0), (20000, 15000))
    assert convertir_donnees_acquisition(75,(100,200),(300,450),(500,600))==(4.5e+24, (300000000000.0, 375000000000.0), (20000, 15000))

def test_distance():
    assert distance([0,0],[1,0]) == 1
    assert distance([1,1],[-2,-2]) == np.sqrt(18)

def test_trouve_theta():
    assert trouve_theta([1,0]) == 0
    assert trouve_theta([0,-1]) == -np.pi*0.5 or 1.5*np.pi
    assert trouve_theta([np.sqrt(3)*0.5, -0.5]) == -np.pi/6 - 1e-16


def test_cylindrique_to_cartesien():
    assert [-1.1,-1e-7] <= list(cylindrique_to_cartesien([1, np.pi])) <= [-0.9,1e-7]
    assert [-0.1, 0.4] <= list(cylindrique_to_cartesien([0.5, 0.5*np.pi])) <= [0.1, 0.6]

def test_cartesien_to_cylindrique():
    assert [0.9,-0.1] <= list(cartesien_to_cylindrique([1,0])) <= [1.1, 0.1]
    assert [0.9, 2*np.pi/3 - 0.1] <= list(cartesien_to_cylindrique([-0.5,np.sqrt(3)*0.5])) <= [1.1, 2*np.pi/3 + 0.1]

def test_Force():
    eps=0.00001
    vect_force=Force(Terre,Soleil)
    attendu=np.array([3.5239142399999996e+22,0.00000000e+00])
    for i in range(2):
        assert (abs(vect_force[i]-attendu[i])<eps)

def test_F_totale():
    eps=0.00001
    vect_force=F_totale([Terre,Soleil,Mars,Mercure,Venus],0)
    attendu=np.array([3.524027187203099e+22,0.00000000e+00])
    for i in range(2):
        assert (abs(vect_force[i]-attendu[i])<eps)

def test_calcul_etat_suivant_general():
    eps=0.00001
    etat_planete=calcul_etat_suivant_general(liste_astres_attracteurs,3)
    attendu=[6e24,np.array([-149999961726.7449, -1.07352000e+08]),np.array([ 2.12629195e+01, -2.98200000e+04]),(17, 108, 240)]
    for i in range(1,3):
        for j in range(2):
            assert (abs(etat_planete[i][j]-attendu[i][j])<eps)
    assert etat_planete[3]==attendu[3]
    assert etat_planete[0]==attendu[0]

